import { Injectable } from '@angular/core';
import {MatSnackBar} from '@angular/material/snack-bar';
export interface admin {
  value: string;
  viewValue: string;
}

@Injectable({
  providedIn: 'root'
})
export class NotificationService {

  constructor(private _snackbar:MatSnackBar) { }
notify(message:string,action:string){
  this._snackbar.open(message,action,{
    duration:10000,
  })
}
delete_userrecord(message:string,action:string){
    this._snackbar.open(message,action,{
      duration:2000,
    })
  
}
}
