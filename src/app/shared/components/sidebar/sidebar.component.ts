import { Component, OnInit } from '@angular/core';
import {  ActivatedRoute,ParamMap,Router } from '@angular/router';
import * as jwt_decode from "jwt-decode";

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  role:string;
 token:string;
 email:string;
  constructor(private router:Router) { }

  ngOnInit(): void {
    this.token = localStorage.getItem('userstoken');
    if(!this.token){
      this.router.navigate(['/auth/login']);
    }else{
      localStorage.setItem('current-page', 'Admin-dashboard');
      const k = localStorage.getItem('userstoken');

      let payload
      if (this.token) {
        payload = this.token.split('.')[1]
        payload = window.atob(payload)
        // console.log(JSON.parse(payload));
        var decoded = jwt_decode(k);
        this.role = decoded['role'].split('-')[1];
        this.email = decoded['email']
        // console.log(decoded);
      }
    }
  }

}
