import { Component, OnInit ,Output,EventEmitter} from '@angular/core';
// import { EventEmitter } from 'protractor';
import {AuthGuardService} from '../../../auth-guard.service';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  title = 'Admin Dashboard';
  opened=false;
  @Output() toggleopen: EventEmitter<any>=new EventEmitter();
    constructor(private guard:AuthGuardService) {
       console.log("hello");
      }

  ngOnInit(): void {
  }
toggle(){
  this.toggleopen.emit();
}
signout(){
  console.log("hai");
this.guard.logout();
}
}
