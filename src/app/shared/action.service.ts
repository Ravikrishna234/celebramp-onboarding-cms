import { Injectable } from '@angular/core';
import { FormGroup,FormControl,Validators} from '@angular/forms';
import {AuthenticationService} from 'src/app/authentication.service';
@Injectable({
  providedIn: 'root'
})
export class ActionService {
  usrcall:any[];
  constructor(private auth:AuthenticationService) { }
  form:FormGroup = new FormGroup({
    first_name:new FormControl('',[Validators.required]),
    last_name:new FormControl('',[Validators.required]),
    email:new FormControl('',[
      Validators.required,
    Validators.email,
    ]),
    password:new FormControl('',[Validators.required]),
    mobile:new FormControl('',[Validators.required,
      ,Validators.minLength(10)]),
    gender:new FormControl('',[Validators.required]),
    adminControl:new FormControl('', [Validators.required])


  })
  onClear(){
    // this.form.value['first_name']='',
    // this.form.value['first_name']='',
    // this.form.value['first_name']='',
    // this.form.value['first_name']='',
    // this.form.value['first_name']='',
    this.form.reset();
  
  }
  OnSubmit(form:any){
    console.log(form);
        this.auth.register(form).subscribe(
      (c) => {
        console.log(c);
      },
      err => {
        console.error(err);
      }
    );

    
    this.onClear();   
  
  
  }
  populateform(user){
    // var usr = user;
    // this.usrcall[0].push(usr);
    // this.form.setValue(user);
    this.form.value['first_name'] = user.first_name;
    // this.ActionService.form.value['email'] = id.email;
    // this.ActionService.form.value['last_name'] = id.first_name;
    // this.ActionService.form.value['adminControl'] = "Qualityexecutive";
    // this.ActionService.form.value['password'] = id.first_name;
    // this.ActionService.form.value['mobile'] = "7680922162";
    // this.ActionService.form.value['gender'] = "1";*/
    console.log(typeof(user));
  }
}
