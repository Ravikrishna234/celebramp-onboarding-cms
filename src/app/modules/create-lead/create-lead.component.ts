import { Component, OnInit } from '@angular/core';
import { ActivatedRoute ,Router} from '@angular/router';
import { FormGroup,FormControl,Validators} from '@angular/forms';
import {AuthenticationService} from '../../authentication.service';
import {saveAs} from 'file-saver';
import { ThrowStmt } from '@angular/compiler';
import * as jwt_decode from "jwt-decode";

@Component({
  selector: 'app-create-lead',
  templateUrl: './create-lead.component.html',
  styleUrls: ['./create-lead.component.css']
})
export class CreateLeadComponent implements OnInit {
  existfile:any;
  name:string
  i:any;
  decoded;
  file:any;
  disabled=true;
   approval;
   token:string;
   buttondisable:boolean;
   role:string;
   email:string;
   store:FormGroup;
   checkuploadstatus=['pancard','aadharcard','gst','brd'];
    constructor( private route:ActivatedRoute,
    public auth:AuthenticationService,private router:Router ) { }
    form:FormGroup = new FormGroup({
      Name:new FormControl('',[Validators.required]),
      almamater:new FormControl('',[Validators.required]),
      email:new FormControl('',[
        Validators.required,
      Validators.email,
      ]),
      mobile:new FormControl('',[Validators.required,
        ,Validators.minLength(10)]),
        pancard:new FormControl({value:'',disabled:true},[Validators.required]),
        aadharcard:new FormControl({value:'',disabled:true},[Validators.required]),
        gst:new FormControl({value:'',disabled:true},[Validators.required]),
        brd:new FormControl({value:'',disabled:true},[Validators.required]),
        appl_id:new FormControl('',[Validators.required]),
      avgsales:new FormControl('',[Validators.required]),
      city:new FormControl('',[Validators.required]),
      companyName:new FormControl('',[Validators.required]),
      designs:new FormControl('',[Validators.required]),
      dial_code:new FormControl('',[Validators.required]),
      establishment:new FormControl('',[Validators.required]),
      experience:new FormControl({value:'',disabled:true},[Validators.required]),
      awards:new FormControl({value:'',disabled:true},[Validators.required]),

      foreignclients:new FormControl('',[Validators.required]),
      status:new FormControl({value:'',disabled:true},[Validators.required]),
      totaldesigners:new FormControl('',[Validators.required]),
      facebook:new FormControl('',[Validators.required]),
      twitter:new FormControl('',[Validators.required]),
      instagram:new FormControl('',[Validators.required]),
      weburl:new FormControl('',[Validators.required]),
      country:new FormControl('',[Validators.required]),
      streetAddress:new FormControl('',[Validators.required]),
      state:new FormControl('',[Validators.required]),
      zipcode:new FormControl('',[Validators.required]),

  
    })
  ngOnInit(): void {
    
    this.token = localStorage.getItem('userstoken');
    if(!this.token){
      this.router.navigate(['/auth/login']);
    }else{
      localStorage.setItem('current-page', 'Admin-dashboard');
      const k = localStorage.getItem('userstoken');
      console.log("hello")

      let payload
      if (this.token) {
        payload = this.token.split('.')[1]
        payload = window.atob(payload)
        console.log(JSON.parse(payload));
         this.decoded = jwt_decode(k);
        this.role = this.decoded['role'].split('-')[1];
        console.log(this.decoded['first_name']);
        this.email = this.decoded['email'];
        console.log(this.email);
      }
    }
    this.i = this.route.snapshot.paramMap.get('app_id');
    console.log(this.i);
    this.auth.getbasicid(this.i).subscribe(
      (c)=> {
        console.log(c);
        if(c['Approval']==1){
          this.approval = true;
        }else{
          this.approval = false;
        }
        console.log(this.form);
        this.editEmployee(this.form,c);
       this.checkupdatestatus(this.form)
        this.auth.getsocialid(c.email).subscribe(
          (d) =>{
            console.log(d);
            this.editEmployee1(this.form,d);
            this.auth.getaddressid(c.email).subscribe(
            (e) => {
              console.log(e);
              this.editEmployee2(this.form,e);

            },
            (error)=>{console.log(error);}
            )
          },
          (error)=>{
            console.log(error);
          }
        )

      },
      (error)=>{
        console.log(error);
      }
    )

  }
  editEmployee(form:FormGroup,value){
form.patchValue({
  appl_id:value.appl_id,
  email:value.email,
  Name:value.Name,
  experience:value.experience,
  awards:value.awards,
  almamater:value.almamater,
 mobile:value.phonenumber,
   File:value.File,
  avgsales:value.avgsales,
  city:value.city,
  companyName:value.companyName,
  designs:value.designs,
  dial_code:value.dial_code.split(':')[1],
  establishment:value.establishment.split(':')[1],
  foreignclients:value.foreignclients,
  status:value.status,
  totaldesigners:value.totaldesigners,
  aadharcard:value.aadhar_card,
  brd:value.brd,
  gst:value.gst,
  pancard:value.pan_card
  // password
})
let n = this.form.controls['appl_id'].value;
this.name = n;
console.log(this.name);
  }
  editEmployee1(form:FormGroup,values){
    form.patchValue({
      facebook:values[0]['facebook'],
      twitter:values[0]['twitter'],
      instagram:values[0]['instagram'],
      weburl:values[0]['weburl']
    })
  }
  editEmployee2(form:FormGroup,values) {
    form.patchValue({
      country:values.country,
      zipcode:values.zipcode,
      streetAddress:values.streetAddress,
      state:values.state,
    })
  }

OnSubmit(){}
OnDownload(file:string){
  let filename = this.form.controls[file].value;
  let app_id = this.form.controls['appl_id'].value;
// this.auth.downloadimage(filename,app_id).subscribe()
this.auth.a(filename,app_id).subscribe(
  (c)=>{
    saveAs(c,filename);
  },(error)=>{
    console.log(error);
  }
);

}
UpdateStatus(){
  let app_id = this.form.controls['appl_id'].value;
  this.auth.updatestatusapproval(app_id).subscribe(
    (c)=>{
      console.log(c['Approval']);
      
      this.approval = true;
    },(error)=> {
      console.log(error);
      this.approval = false
    }
  )}
  checkupdatestatus(group:FormGroup){
  Object.keys(group.controls).forEach((key:string)=>{
    var abstractcontrol = this.form.get(key);
    this.store = this.form;
      if(this.checkuploadstatus.includes(key)) {
        console.log(this.form.controls[key].value!=null);
        if(this.form.controls[key].value != null){
        this.buttondisable = true;  
        }else {
          console.log("Hello");
          this.buttondisable = false;
        
          return false;
        }
    }
  })
}
  // let download = new Promise(function(resolve,reject){
  //   if(this.downloadfile(filename,app_id)){resolve(this.file)}
  //   else{console.log("issue taken place try again")}
  // });
  // download.then(res=>{
  //   console.log(res);
  //   saveAs(res,filename);
  // })

disableform(){
  if( this.disabled){
    this.form.disable();
  }else{
    this.form.enable();
  }
}
}
