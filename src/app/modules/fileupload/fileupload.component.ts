import { Component, OnInit,Input,ElementRef,ViewChild } from '@angular/core';
import { FormGroup,FormControl,Validators} from '@angular/forms';
import { MatDialogRef} from '@angular/material/dialog';
import {NotificationService} from '../../shared/notification.service'; 
import {admin} from '../../shared/notification.service'; 
import {ActionService} from '../../shared/action.service';
import { AuthenticationService } from '../../authentication.service';
import {saveAs} from 'file-saver';
import {showLeads} from 'src/app/authentication.service';
import { ThrowStmt } from '@angular/compiler';
import{ environment} from 'src/environments/environment';
import { HttpClient,HttpHeaders, HttpEventType } from '@angular/common/http'

@Component({
  selector: 'app-fileupload',
  templateUrl: './fileupload.component.html',
  styleUrls: ['./fileupload.component.css']
})
export class FileuploadComponent implements OnInit {
  pdferror :string;
  filerror:string;
  progress=0;
  message:string;
  res:any;
  fileres:any;
  files = [];
  submitted:boolean;
  selectedFile:File = null;
  showLeads:showLeads[];
  newarr = [];
  i=0;
  @Input() name1:string;
  constructor(private auth: AuthenticationService,
    private elementRef: ElementRef,
     public notification:NotificationService,
     private http:HttpClient) {}
  ngOnInit(): void {
    console.log(this.name1);
    this.auth.basicleads().subscribe(
      x=>{
        if(x) {
          this.showLeads = x;
          console.log(this.showLeads);
           x.forEach(c=>{
            this.newarr.push(c.city);
            })
          }
        },(error)=>{
          console.log(error);
        }
    )};
  form:FormGroup = new FormGroup({
    // CName:new FormControl({disabled:true},[Validators.required]),
    // status:new FormControl('',[Validators.required]),
  })
@ViewChild('fileInput',{static:false}) fileInput:ElementRef;
@ViewChild('fileInput1',{static:false}) fileInput1:ElementRef;
@ViewChild('fileInput2',{static:false}) fileInput2:ElementRef;
@ViewChild('fileInput3',{static:false}) fileInput3:ElementRef;

update() {
  var imageblob = this.fileInput.nativeElement.files[0];
    var imageblob1 = this.fileInput1.nativeElement.files[0];
    var imageblob2 = this.fileInput2.nativeElement.files[0];
    var imageblob3 = this.fileInput3.nativeElement.files[0];
    console.log(this.fileInput);
    const formData = new FormData();
    var file = new FormData();
    // try {
    if(imageblob){

      console.log(imageblob3.name);

      const mimetype = imageblob['name'].split('.');
      const mimetype1 = imageblob1['name'].split('.');
      const mimetype2 = imageblob2['name'].split('.');
      const mimetype3 = imageblob3['name'].split('.');
     
      console.log(mimetype);

      var extensions = ['png','jpg','jpeg','PNG','JPG','JPEG'];

      this.res = ((extensions.includes(mimetype[1].toLowerCase())  &&
      extensions.includes(mimetype1[1].toLowerCase() ) && 
      extensions.includes( mimetype2[1].toLowerCase() ) &&
      extensions.includes( mimetype3[1].toLowerCase() )))
      console.log(this.res);
     if(mimetype.length >= 3){
      this.filerror = "Specify correct extension";  

     } 
     
     else if (!this.res)
     
   {
      this.filerror = "Only image allowed";  
      this.pdferror = "";
     } 
     else {
      
      this.filerror = "";  
      // Object.defineProperty(imageblob, 'name', {
      //   writable: true,
      //   value: this.name1+"-pancard"+"."+mimetype[1]
      // });
      
    
      console.log(imageblob);
    file.append('file',imageblob);
    file.append('file1',imageblob1);
    file.append('file2',imageblob2);
    file.append('file3',imageblob3);
      this.http.post(environment.pdfurl+`/setname/`+this.name1,this.name1).subscribe(
        (c) => {
          console.log(true);
        },(error)=> {
          console.log(error);
        }
      )
            this.auth.insertpdf(file).subscribe(
              (res)=>{ 
                this.fileres = res;
                console.log(this.fileres);
                this.auth.updatebasic(this.fileres['message'][0]['AadharCard'],
                this.fileres['message'][0]['PanCard'],
                this.name1,
                this.fileres['message'][0]['GstLicense'],
                this.fileres['message'][0]['BussinessRegistrationDocument']).subscribe(
                 (c) => {
                   console.log(c);
                   this.notification.notify('Submitted','');

                 },(error) => {
                   console.log(error);
                 })
              },(error)=>{
                console.log(error);
              });
                // see below
               
                
            

    // this.http.post(environment.pdfurl+`/upload`,file,{
    //   reportProgress:true,
    //   observe:'events',
      
    // }).subscribe(
    //     // this.auth.insertpdf(file).subscribe(
    //   (events) => {
    //     if(events) {
    //       console.log(this.name1  );
    //       if(events['body']){
    //       this.files.push(events['body']);
    //       console.log(this.files[0]['uploadName']);
    //       }
    //       // console.log(this.files[this.files.length-1]);
    //       this.notification.notify('Submitted','');
    //       if(events.type === HttpEventType.UploadProgress){
    //         this.progress = Math.round(events.loaded/events.total*100);
    //         console.log("Upload Progress"+Math.round(events.loaded/events.total*100)+"%");
    //       } if(events['body']){
    //         console.log(this.progress);
    //         // upload with top
    //       this.auth.updatebasic(this.files[0]['uploadName'],
    //        this.name1,
    //         this.form.value['status']).subscribe(
    //         (d) => {
    //           console.log(d);
    //         },(error) => {
    //           console.log(error);
    //         })
    //       this.name1 = '';
    //       this.form.controls['status'].setValue('');
    //       this.files = [];

    //     }
    //   }
      

    //   },(error)=>{
    //     console.log(error);
    //     this.notification.notify('Error on Upload','');

    //   })
    //   ;
     }


   

  }
  else{
    console.log("Hai"); 
    this.pdferror = "File is required";
  }
// }
  // catch(_e){
  //   this.filerror = "Upload all files";  

  // }
}
delete() {
  this.form.reset();
}
download(){
  var filename = this.files[1].uploadName;
  console.log(filename);
  let id="1";
  this.auth.downloadimage(filename,id).subscribe(
    (c)=>{
      saveAs(c,filename);
      console.log(c);
    } ,(error)=>{
      console.log(error);
    })
}

  // onFileSelect(event){
  //   console.log(event);
  //   if (event.target.files.length > 0) {
  //     this.selectedFile = <File>event.target.files[0];
  //     this.form.get('file_pdf').setValue(this.selectedFile);
  //   }
  //   console.log(event.target.files);
  // }
  // OnUpload() {
  //   const imageblob = this.fileInput.nativeElement.files[0];
  //   const file = formData();
  //   file.set('file',imageblob);
  //   var formData = new formData(this.form);
  //   // formData.append("file_pdf",this.form.get('file_pdf').value)
  //   this.submitted = true;
  //   console.log(this.form.get('file_pdf').value);
  //   this.auth.insertpdf(file).subscribe(
  //     (c) => {
  //       if(c) {
  //         console.log(c);
  //         this.notification.notify('Submitted','');

  //       }

  //     });


  // }

  

}
