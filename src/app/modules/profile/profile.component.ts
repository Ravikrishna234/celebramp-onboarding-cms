import { Component, OnInit } from '@angular/core';
import { FormGroup,FormControl,Validators} from '@angular/forms';
import {AuthenticationService} from 'src/app/authentication.service';
import {  ActivatedRoute,ParamMap,Router } from '@angular/router';
import * as jwt_decode from "jwt-decode";

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})

export class ProfileComponent implements OnInit {
   decoded;
  role:string;
  email:string;
  token:string;
  disabled=true;
  constructor(private router:Router) { }
  ngOnInit()  {
    this.token = localStorage.getItem('userstoken');
    if(!this.token){
      this.router.navigate(['/auth/login']);
    }else{
      localStorage.setItem('current-page', 'Admin-dashboard');
      const k = localStorage.getItem('userstoken');
      console.log("hello")

      let payload
      if (this.token) {
        payload = this.token.split('.')[1]
        payload = window.atob(payload)
        console.log(JSON.parse(payload));
         this.decoded = jwt_decode(k);
        this.role = this.decoded['role'].split('-')[1];
        console.log(this.decoded['first_name']);
        this.email = this.decoded['email']
      }
    }
    console.log(this.form);
    this.form.controls['first_name'].setValue(this.decoded['first_name']);
    this.form.controls['last_name'].setValue(this.decoded['last_name']);
    this.form.controls['email'].setValue(this.decoded['email']);
    this.form.controls['role'].setValue(this.decoded['role']);

  }

  adminArr = [];
 
  form:FormGroup = new FormGroup({
    first_name:new FormControl('',[Validators.required]),
    last_name:new FormControl('',[Validators.required]),
    email:new FormControl('',[
      Validators.required,
    Validators.email,
    ]),
    role:new FormControl({value:'',disabled:true},Validators.required)
 

  })
OnSubmit(){}
}
