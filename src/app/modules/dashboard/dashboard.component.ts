import { Component, OnInit } from '@angular/core';
import {AuthenticationService} from 'src/app/authentication.service';
import {  ActivatedRoute,ParamMap,Router } from '@angular/router';
import * as jwt_decode from "jwt-decode";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  token:string;
  user = "";
  Allocated=[];
  Totalleads = [];
  executives = [];
  notAllocated:any;
  users = [];
  constructor(private router:Router,
    private route: ActivatedRoute,
    public auth:AuthenticationService) {
    this.route.paramMap.subscribe(
      params => {
        const k = +params.get('token');
        console.log(k);
      }
    )
    localStorage.setItem('current-page', 'Admin-dashboard');
    let k = this.route.snapshot.queryParams.token;
      console.log(k);
    // localStorage.setItem('access_token',k)

    if (localStorage.getItem('userstoken')) {
      k = localStorage.getItem('userstoken');
     }
     console.log(k);
     if(k){
      const token =k;
      let payload
      if (token) {
        payload = token.split('.')[1]
        payload = window.atob(payload)
        console.log(JSON.parse(payload));
        var decoded = jwt_decode(token);
        console.log(decoded);
        this.user=decoded["email"];
        var query = {email:this.user};
        console.log(query);
      }
  }
} 
  ngOnInit(): void {
    this.token = localStorage.getItem('userstoken');
    if(!this.token){
      this.router.navigate(['/auth/login']);
    }
    this.auth.showAll().subscribe(
      (x) => {
        console.log(x);
        this.users = x;
        this.executives = x.filter(function(executives){
          if(executives.role === "admin-executive"){
            return true;
          }
        })
        console.log(this.executives);
      },
      (error)=>{
        console.log(error);
      }
    )
    this.auth.basicleads().subscribe(
      (x)=> {
        console.log(x);
        this.Totalleads= x;
        this.Allocated = x.filter(allocated => allocated.Allocated )
        this.notAllocated = this.Totalleads.length - this.Allocated.length;
      },
      (error)=> {
        console.log(error);
      }
    )
  }

}
