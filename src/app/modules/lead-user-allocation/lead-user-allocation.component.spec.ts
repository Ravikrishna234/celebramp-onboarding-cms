import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LeadUserAllocationComponent } from './lead-user-allocation.component';

describe('LeadUserAllocationComponent', () => {
  let component: LeadUserAllocationComponent;
  let fixture: ComponentFixture<LeadUserAllocationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LeadUserAllocationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LeadUserAllocationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
