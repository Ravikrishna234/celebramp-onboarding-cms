import { Component, OnInit ,Inject} from '@angular/core';
import { FormGroup,FormControl,Validators,FormArray,FormBuilder,ValidatorFn} from '@angular/forms';
import {AuthenticationService} from 'src/app/authentication.service';
import {executive,showLeads} from 'src/app/authentication.service';
import {MatDialog, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
export interface DialogData {
  executive:string,
  user:[]
  }
@Component({
  selector: 'app-lead-user-allocation',
  templateUrl: './lead-user-allocation.component.html',
  styleUrls: ['./lead-user-allocation.component.css']
})

export class LeadUserAllocationComponent implements OnInit {
  allocations = [];
  assign = [];
  constructor(public auth:AuthenticationService,
    public formBuilder:FormBuilder,
    public dialog: MatDialog) {}
 
  form:FormGroup = this.formBuilder.group({
    executives:new FormControl('', [Validators.required]),
    users:new FormControl('', [Validators.required])

  })
  executiveArray=[];
  allocate=false
  select_all = false;
  checkallocations:boolean;
  usersArray=[];
  userarea:string;
  executives:executive[];
  showLeads:showLeads[];
  selectedcontrol1:'';
   newarr = [];
   cities:Object[]
  ngOnInit(): void {
this.auth.getlead().subscribe(
  x=>{
    if(x){
      this.executives = x;
      console.log(this.executives);

    }
  },(error)=>{console.log(error); })
  this.auth.basicleads().subscribe(
    x=>{
      if(x) {
        this.showLeads = x;
        console.log(this.showLeads);
         x.forEach(c=>{
          this.newarr.push(c.city);
          })
          this.newarr.push("Nizamabad");
          // console.log(this.newarr);

          this.cities = Array.from(new Set(this.newarr));
          // console.log(this.cities);
      }
    }
  )
 }

OnSubmit(){
  this.assign = [];
  this.userarea = this.form.get('users').value;
  Object.keys(this.form.controls).forEach((key:string)=>{
    if(key == "users") {
     this.showLeads.forEach(c=>{
       if(c.city === this.form.get(key).value ){
         console.log(c.Allocated);
         if(!c.Allocated){
         this.checkallocations = true;
         var users = {Name:c.Name,city:c.city,Ph:c.phonenumber,Email:c.email,checked:false};
         this.usersArray.push(users);
       }
      }
    });
    console.log(this.usersArray);
        if(this.usersArray.length==0){
          console.log(this.usersArray);
        this.allocate = true;

        
       }else {
        this.allocate = false;
       }
    
     this.allocations = this.usersArray;
     this.usersArray = [];
    console.log(this.usersArray);
    }
  })
  console.log(this.form);
}
onClear(){
  this.form.reset();
}
submit(){
if(this.assign.length>0){
  this.notificationdialog();
}
}
updateItem(event,item) {
  this.allocations.map((users)=>{
  if(users.Email === item.Email){
    if(item.checked){
    console.log(item.Email);
    item.checked = true;
    this.assign.push(item);
    }else{
      item.checked = false;
      this.assign.splice(item,1);
    }

  }
  }
  )
console.log(this.assign);
}
updateAll(){
  console.log(this.select_all+"a");
  if(this.select_all === true){
    this.allocations.map((users)=>{
      users.checked=true;
    });
    
  }else {
    this.allocations.map((users)=>{
      users.checked=false;
    });
  }
}
notificationdialog(){
  this.executiveArray = [];
  this.executives.forEach(executive=>{
    if(executive.id===this.form.get('executives').value){
      this.executiveArray.push(executive);
    }
  })
  console.log(this.executiveArray);
  const dialogRef = this.dialog.open(VerificationDialog, {
    width: '250px',
    data: {executive: this.executiveArray, user: this.assign}

  });

  dialogRef.afterClosed().subscribe(result => {
    console.log('The dialog was closed');
   
  });
}


}
@Component({
  selector: 'verificationdialog',
  templateUrl: 'verificationdialog.html',
})
export class VerificationDialog {
  constructor(public auth:AuthenticationService,
    public dialogRef: MatDialogRef<VerificationDialog>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}
    executive = this.data.executive;
    associate = this.executive[0]["email"];
    elements = this.data.user.length;
    show:boolean;
    users:any;
  onNoClick(): void {
    this.dialogRef.close();
  }
  onYesClick(){
    this.show = true;
    console.log(this.data.user);
    console.log(this.executive[0]["email"]);
    this.auth.allocatedleads(this.data.user,this.executive).subscribe(
      (x)=>{
        console.log(x);
       
      },
      (error)=>{
        console.log("Error while allocating")
      }
    )
    this.dialogRef.close();

  }
}
