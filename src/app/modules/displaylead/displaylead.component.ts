import { Component, OnInit,ViewChild } from '@angular/core';
import {AuthenticationService} from 'src/app/authentication.service';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { DataSource } from '@angular/cdk/table';
import {Router} from '@angular/router';
import * as jwt_decode from "jwt-decode";
import {from } from 'rxjs';
import { filter, toArray } from 'rxjs/operators';
export interface showLeads {
  email: string
  phonenumber: string
  companyName: string
  Name: string
  city: string
}
@Component({
  selector: 'app-displaylead',
  templateUrl: './displaylead.component.html',
  styleUrls: ['./displaylead.component.css']
})

export class DisplayleadComponent implements OnInit {
  displayedColumns: string[] = ['Name','city', 'email','phonenumber','Type of Allocation','ExecutiveName','action'];
  dataSource1;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
userdetails:showLeads[];
decoded;
token:string;
role:string;
email:string;
  details:any;
  allocatedArray=[]
  constructor(public auth:AuthenticationService,public router:Router) { }

  ngOnInit(): void {
    this.token = localStorage.getItem('userstoken');
    if(!this.token){
      this.router.navigate(['/auth/login']);
    }else{
      localStorage.setItem('current-page', 'Admin-dashboard');
      const k = localStorage.getItem('userstoken');
      console.log("hello")

      let payload
      if (this.token) {
        payload = this.token.split('.')[1]
        payload = window.atob(payload)
        console.log(JSON.parse(payload));
         this.decoded = jwt_decode(k);
        this.role = this.decoded['role'].split('-')[1];
        console.log(this.decoded['first_name']);
        this.email = this.decoded['email']
        console.log(this.role);
      }
    }
    this.auth.basicleads().subscribe(
      (c)=>{
        console.log(c);
        this.userdetails=c;
        if(this.role == "executive"){
        const source = from(this.userdetails);
        source.pipe(
          filter(member=> member['Allocatedexecutivename'] == this.email),
          toArray()
        )
        .subscribe(res=>{
          console.log(res);
          this.userdetails = res;
        })
      }
        this.dataSource1 = new MatTableDataSource(this.userdetails);
        this.dataSource1.paginator = this.paginator;
        this.dataSource1.sort = this.sort;
      },
      (error)=>{
        console.log(error);
      }
    )

}




 
  
 
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource1.filter = filterValue.trim().toLowerCase();

    if (this.dataSource1.paginator) {
      this.dataSource1.paginator.firstPage();
    }
  }
  onedit(id){
    this.router.navigate(['edit_lead/'+id.appl_id])
  }
}

