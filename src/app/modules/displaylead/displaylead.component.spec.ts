import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DisplayleadComponent } from './displaylead.component';

describe('DisplayleadComponent', () => {
  let component: DisplayleadComponent;
  let fixture: ComponentFixture<DisplayleadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DisplayleadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DisplayleadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
