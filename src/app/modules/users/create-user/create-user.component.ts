import { Component, OnInit } from '@angular/core';
import { FormGroup,FormControl,Validators} from '@angular/forms';
import { MatDialogRef} from '@angular/material/dialog';
import {NotificationService} from '../../../shared/notification.service'; 
import {admin} from '../../../shared/notification.service'; 
import {ActionService} from '../../../shared/action.service';
@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {
  i:any;
  form:FormGroup = new FormGroup({
    first_name:new FormControl('',[Validators.required]),
    last_name:new FormControl('',[Validators.required]),
    email:new FormControl('',[
      Validators.required,
    Validators.email,
    ]),
    password:new FormControl('',[Validators.required]),
    mobile:new FormControl('',[Validators.required,
      ,Validators.minLength(10)]),
    gender:new FormControl('',[Validators.required]),
    role:new FormControl('', [Validators.required])


  })
  hide = true;
  constructor(private notification:NotificationService,
    private Dialogref:MatDialogRef<CreateUserComponent>,
    public ActionService:ActionService) { }
    admins: admin[] = [
      {value: 'admin-executive', viewValue: 'Executive'},
      {value: 'admin-system', viewValue: 'System-admin'}
    ];
  ngOnInit(): void {
    console.log(this.ActionService.form.value);
    this.form.controls['first_name'].setValue(this.ActionService.form.value['first_name'])
    this.form.controls['email'].setValue(this.ActionService.form.value['email']) 
    this.form.controls['last_name'].setValue(this.ActionService.form.value['last_name']) 
    this.form.controls['adminControl'].setValue(this.ActionService.form.value['adminControl']) 
    this.form.controls['password'].setValue(this.ActionService.form.value['password']) 
    this.form.controls['mobile'].setValue(this.ActionService.form.value['mobile']) 
    this.form.controls['gender'].setValue(this.ActionService.form.value['gender'])     
    
  }
onClear(){
this.ActionService.onClear();
this.form.reset();
  this.Dialogref.close();

}
OnSubmit(){
  console.log(this.form.value);
  this.ActionService.OnSubmit(this.form.value);
  this.onClear();
  this.notification.notify('Submitted','');
 


}

}
