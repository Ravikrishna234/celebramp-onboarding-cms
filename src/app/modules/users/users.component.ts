import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { DataSource } from '@angular/cdk/table';
import { AuthenticationService } from '../../authentication.service';
import {MatDialog,MatDialogConfig} from '@angular/material/dialog';
import {CreateUserComponent } from './create-user/create-user.component';
import {ActionService} from '../../shared/action.service';
import {NotificationService} from '../../shared/notification.service'; 

export interface PeriodicElement {
  name: string;
  position: number;
  weight: number;
  symbol: string;
}
export interface showUsers {
  id: number
  first_name: string
  last_name: string
  email: string
  password: string
}
const ELEMENT_DATA: PeriodicElement[] = [
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
  {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'},
  {position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li'},
  {position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be'},
  {position: 5, name: 'Boron', weight: 10.811, symbol: 'B'},
  {position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C'},
  {position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N'},
  {position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O'},
  {position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F'},
  {position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne'},
];

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
 
  
  displayedColumns: string[] = ['id', 'first_name', 'last_name', 'email','action'];
  dataSource;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
userdetails:showUsers[];
  constructor(private auth: AuthenticationService,
    private dialog:MatDialog,
    public ActionService:ActionService,
    public notification:NotificationService) {}
  // Create 100 users
    // const users = Array.from({length: 100}, (_, k) => createNewUser(k + 1));

    // Assign the data to the data source for the table to render
    // this.dataSource = new MatTableDataSource(users);
  

  ngOnInit() {
    this.auth.showAll().subscribe(
      (c) => {
        console.log(c);
        this.userdetails=c;
        this.dataSource = new MatTableDataSource(this.userdetails);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      },
      err => {
        console.error(err)
      });

   
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }
  openCreatedialog(){
    
const Dialogconfig  = new MatDialogConfig();
Dialogconfig.disableClose = true;
Dialogconfig.autoFocus = true;
Dialogconfig.width = "60%";  
this.dialog.open(CreateUserComponent,Dialogconfig);
  }
  ondel(id:any){
    console.log(id);
    this.auth.deleteuser(id).subscribe(
      (c) => {
        if(c) {
          this.notification.delete_userrecord('Deleted a record','');
        }
      },
      (error)=>{
        console.log(error);
      }
    
    )
  }
  onedit(id:any){
    // console.log(this.ActionService.form);
    this.ActionService.populateform(id);
    // console.log(id.first_name);
    this.ActionService.form.value['first_name'] = id.first_name;
    this.ActionService.form.value['email'] = id.email;
    this.ActionService.form.value['last_name'] = id.first_name;
    this.ActionService.form.value['adminControl'] = "Qualityexecutive";
    this.ActionService.form.value['password'] = id.first_name;
    this.ActionService.form.value['mobile'] = "7680922162";
    this.ActionService.form.value['gender'] = "1";
    const Dialogconfig  = new MatDialogConfig();
Dialogconfig.disableClose = true;
Dialogconfig.autoFocus = true;
Dialogconfig.width = "60%";  
this.dialog.open(CreateUserComponent,Dialogconfig);
  
  }
}

/** Builds and returns a new User. */
// function createNewUser(id: number): UserData {
//   const name = NAMES[Math.round(Math.random() * (NAMES.length - 1))] + ' ' +
//       NAMES[Math.round(Math.random() * (NAMES.length - 1))].charAt(0) + '.';

//   return {
//     id: id.toString(),
//     name: name,
//     progress: Math.round(Math.random() * 100).toString(),
//     color: COLORS[Math.round(Math.random() * (COLORS.length - 1))]
//   };
// }

