import {Component, OnInit, ViewChild} from '@angular/core';
import { AuthenticationService } from '../../authentication.service';
import {MatPaginator} from '@angular/material/paginator';
import {MatSort} from '@angular/material/sort';
import {MatTableDataSource} from '@angular/material/table';
import { DataSource } from '@angular/cdk/table';
import {  ActivatedRoute,ParamMap,Router } from '@angular/router';
export interface showLeads {
  email: string
  phonenumber: string
  companyName: string
  Name: string
  city: string
}

@Component({
  selector: 'app-leads',
  templateUrl: './leads.component.html',
  styleUrls: ['./leads.component.css']
})

export class LeadsComponent implements OnInit {
  displayedColumns: string[] = ['Name','cnam', 'email','phonenumber','city','action'];
  dataSource1;
  @ViewChild(MatPaginator, {static: true}) paginator: MatPaginator;
  @ViewChild(MatSort, {static: true}) sort: MatSort;
userdetails:showLeads[];
 
  constructor(private auth: AuthenticationService,private router:Router)
    { }

  ngOnInit(): void {
    this.auth.basicleads().subscribe(
      (c)=>{
        console.log(c);
        this.userdetails=c;
        this.dataSource1 = new MatTableDataSource(this.userdetails);
        this.dataSource1.paginator = this.paginator;
        this.dataSource1.sort = this.sort;
      },
      (error)=>{
        console.log(error);
      }
    )
  }
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource1.filter = filterValue.trim().toLowerCase();

    if (this.dataSource1.paginator) {
      this.dataSource1.paginator.firstPage();
    }
  }
  onedit(id:any){
    console.log(id);
    this.router.navigate(['edit_lead/'+id.appl_id])
  }

}
