import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DashboardComponent } from './modules/dashboard/dashboard.component';
import { DefaultComponent} from './layouts/default/default.component';
import { UsersComponent } from './modules/users/users.component';
import{CreateUserComponent} from './modules/users/create-user/create-user.component';
import {CreateLeadComponent} from './modules/create-lead/create-lead.component';
import { AuthSharedModule } from 'projects/auth/src/app/app.module';
import {LeadsComponent} from './modules/leads/leads.component';
import {LeadUserAllocationComponent} from './modules/lead-user-allocation/lead-user-allocation.component';
import{DisplayleadComponent} from './modules/displaylead/displaylead.component';
import {ProfileComponent} from './modules/profile/profile.component';
import {FileuploadComponent} from './modules/fileupload/fileupload.component';
const routes: Routes = [
  { path: 'auth', loadChildren: '../../projects/auth/src/app/app.module#AuthSharedModule' },
  { path: '', component: DefaultComponent,
  children:[{
    path:'',
    component:DashboardComponent
  },
  {
    path: 'Users',
    component: UsersComponent,
  },
  {
      path:'leads',
      component:LeadsComponent,
    },
    {
      path:'allocate_lead',
      component:LeadUserAllocationComponent,
    },
  {
    path:'display_lead',
    component:DisplayleadComponent
  },
  {
    path:'profile',
    component:ProfileComponent
  },
  {
    path:'upload',
    component:FileuploadComponent
  },
  {
    path:'edit_lead/:app_id',
    component:CreateLeadComponent
  }
]
  }]
     
   
  


  // { path: 'Users', component: DashboardComponent,
  // canActivate: [AuthGuardService]
// },

@NgModule({
  imports: [
    RouterModule.forRoot(routes),  
      AuthSharedModule.forRoot(),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
