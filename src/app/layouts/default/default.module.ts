import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DefaultComponent} from './default.component';
import { DashboardComponent } from 'src/app/modules/dashboard/dashboard.component';
import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from 'src/app/modules/users/users.component';
import {LeadsComponent} from 'src/app/modules/leads/leads.component';
import{CreateUserComponent} from 'src/app/modules/users/create-user/create-user.component';
import{LeadUserAllocationComponent} from 'src/app/modules/lead-user-allocation/lead-user-allocation.component';
import{DisplayleadComponent} from 'src/app/modules/displaylead/displaylead.component';
import {ProfileComponent} from 'src/app/modules/profile/profile.component';
import {FileuploadComponent} from 'src/app/modules/fileupload/fileupload.component';
import {CreateLeadComponent} from 'src/app/modules/create-lead/create-lead.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';

import {SharedModule} from 'src/app/shared/shared.module';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatPaginatorModule} from '@angular/material/paginator';
import { MatTableModule } from '@angular/material/table';  
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MaterialModule} from 'src/app/material';
// import {MatLabelModule} from '@angular/material/label';

@NgModule({
  declarations: [
    DefaultComponent,
    DashboardComponent,
    UsersComponent,
    CreateUserComponent,
    LeadsComponent,
    LeadUserAllocationComponent,
    DisplayleadComponent,
    ProfileComponent,
    FileuploadComponent,
    CreateLeadComponent
  ],
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    MatSidenavModule,MatFormFieldModule,MatPaginatorModule,MatTableModule,MatInputModule,MatButtonModule,
    MaterialModule,FormsModule, ReactiveFormsModule,PdfViewerModule,


  ]
})
export class DefaultModule { }
