import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders } from '@angular/common/http'
import { Router } from '@angular/router/';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
interface TokenResponse {
  token: string
}
@Injectable({
  providedIn: 'root'
})
export class AuthGuardService {
  private token: string
  constructor(private router:Router) { }
  public logout(): void {

    this.token = localStorage.getItem('userstoken');
    window.localStorage.removeItem('userstoken')
    this.router.navigate(['/auth/login'])
  }
}
