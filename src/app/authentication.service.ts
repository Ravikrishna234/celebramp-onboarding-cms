import { Injectable } from '@angular/core'
import { HttpClient,HttpHeaders, HttpErrorResponse } from '@angular/common/http'
import { Observable, of, throwError } from 'rxjs'
import { map, catchError } from 'rxjs/operators'
import { Router } from '@angular/router'
import{ environment} from 'src/environments/environment';
import { identifierModuleUrl } from '@angular/compiler'
const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
export interface showUsers {
  id: number
  first_name: string
  last_name: string
  email: string
  password: string
  role:string
}
export interface showLeads {
  email: string
  phonenumber: string
  companyName: string
  Name: string
  city: string
  Allocated:boolean
}
export interface executive {
  id:number,
  first_name:string,
  last_name:string,
  email:string
}
export interface allocateleads {
  Email:number,
  Ph:string,
  city:string,
  checked:string,
  Name:string,
}
export interface social {
  Email:number,
  facebook:string,
  instagram:string,
  twitter:string,
  weburl:string,
}
export interface Address {
  Email:number,
  country:string,
  streetAddress:string,
  zipcode:string,
  state:string,
}
export interface TokenPayload {
  id: number
  first_name: string
  last_name: string
  email: string
  password: string
  role:string
}
@Injectable({
  providedIn: 'root'
})

export class AuthenticationService {
  userup:any;
  constructor(private http:HttpClient) { }
  public handlerror(errorResponse:HttpErrorResponse){
if(errorResponse.error instanceof ErrorEvent){
  console.error("Client side error",errorResponse.error);
  
}else{
  console.error("Server side error",errorResponse);
}
return throwError("There is a problem with the service.")
  }
     
  public showAll(): Observable<any> {
    console.log(environment.apiBaseUrl);
    return this.http.get(environment.apiBaseUrl+`/users/FindMany`)
    .pipe(catchError(this.handlerror));
    // return ;
  }
  public updatestatusapproval(appl_id:string):Observable<any[]>{
    let body = {appl_id:appl_id}
    return this.http.post<any[]>(environment.apiBaseUrl+`/basiclinks/UpdateStatus`,body)
    .pipe(catchError(this.handlerror));

  }
  public getaddressid(email:string):Observable<Address[]>{
    return this.http.get<Address[]>(environment.apiBaseUrl+`/addresslinks/Find_Id/`+email)
    .pipe(catchError(this.handlerror));
  }
  public getsocialid(email:string):Observable<social[]>{
    return this.http.get<social[]>(environment.apiBaseUrl+`/sociallinks/Find_Id/`+email)
    .pipe(catchError(this.handlerror));

  }
  public getbasicid(appl_id:string): Observable<any> {
    console.log(environment.apiBaseUrl);
    return this.http.get<showLeads[]>(environment.apiBaseUrl+`/basiclinks/Find_Id/`+appl_id)
    .pipe(catchError(this.handlerror));
    // return ;
  }
  public basicleads(): Observable<showLeads[]> {
    console.log(environment.apiBaseUrl);
    return this.http.get<showLeads[]>(environment.apiBaseUrl+`/basiclinks/FindLeads`)
    .pipe(catchError(this.handlerror));
    // return ;
  }
  public deleteuser(id:any): Observable<any> {
    console.log(environment.apiBaseUrl);
    return this.http.post(environment.apiBaseUrl+`/users/delete_one`,id)
    .pipe(catchError(this.handlerror));
    // return ;
  }
  public getlead(): Observable<executive[]> {
    console.log(environment.apiBaseUrl);
    return this.http.get<executive[]>(environment.apiBaseUrl+`/users/getleads`)
    .pipe(catchError(this.handlerror));
    // return ;
  }
  public allocatedleads(user:allocateleads[],executive:string): Observable<allocateleads[]>{
    this.userup = {user:user,executive:executive};
    console.log(environment.apiBaseUrl);
    return  this.http.put<allocateleads[]>(environment.apiBaseUrl+`/basiclinks/Updatelead`,this.userup)
    .pipe(catchError(this.handlerror));
  }
  public updatebasic(Aadharcard:string,
    Pancard:string,
    companyName:string,
    Gst:string,
    Brd:string): Observable<any[]>{
   var body = {aadhar_card:Aadharcard,pan_card:Pancard,CName:companyName,gst:Gst,brd:Brd};
   console.log(body);
   return this.http.put<any[]>(environment.apiBaseUrl+`/basiclinks/UpdateStatus`,body)
   .pipe(catchError(this.handlerror));

  }
  public updateexecutive(id:any): Observable<any>{
    console.log(environment.apiBaseUrl);
    return this.http.post(environment.apiBaseUrl+`/users/getexecutive`,id)
    .pipe(catchError(this.handlerror));
  }
  public a(file:String,id:String){
    console.log("hello");
    console.log(environment.apiBaseUrl );
    var body = {filename:file,id:id};
    console.log(body);
    console.log(file);
    console.log("hello")
    return this.http.post(environment.pdfurl+`/download`,body,{
      responseType:'blob',
      headers:new HttpHeaders().append('Content-Type', 'application/json' )
    });
  }  
  public downloadimage(file:String,id:String){
      console.log(environment.apiBaseUrl );
      var body = {filename:file,id:id};
      console.log(body);
      console.log(file);
      console.log("hello")
      return this.http.post(environment.pdfurl+`/download`,body,{
        responseType:'blob',
        headers:new HttpHeaders().append('Content-Type', 'application/json' )
      });
    }
  public insertpdf(file:any){
    console.log(environment.apiBaseUrl );
   
    console.log(file);
    console.log("hello")
    return this.http.post(environment.pdfurl+`/upload`,file)
    .pipe(catchError(this.handlerror));
  }
  public register(user: TokenPayload): Observable<any> {
    console.log(user);
    return this.http.post(environment.apiBaseUrl+`/users/register`, user)
    .pipe(catchError(this.handlerror));
    // return ;
  }
 
}
