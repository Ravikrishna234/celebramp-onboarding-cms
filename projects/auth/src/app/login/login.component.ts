import { Component, OnInit } from '@angular/core';
import { HttpClient } from "@angular/common/http";
import { Router, ActivatedRoute, RoutesRecognized } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { environment } from 'src/environments/environment';
import { AuthenticationService, TokenPayload,UserDetails } from '../authentication.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email: string;
  password: string;
  token: string;
  bg;
  myForm: FormGroup;
  formBuilder: FormBuilder;
  error;
  constructor(
    private auth:AuthenticationService,
    private httpClient: HttpClient,
     private router: Router, 
     private route: ActivatedRoute) {
    // this.route.params.subscribe(res=>console.log(res))
    this.formBuilder = new FormBuilder();
    let emailregex: RegExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    this.myForm = this.formBuilder.group({
      'email': [null, [Validators.required, Validators.pattern(emailregex)]],
      'password': [null, [Validators.required]],
    });
    this.bg = {
      "token": '',
      "role": ''
    };
  }
  ngOnInit(): void {
    this.token = localStorage.getItem('userstoken');
    // if(this.token){
    //   this.router.navigate(['']);
    // }
  }
onSubmit(eventDetails){
  if(eventDetails.key=="Enter" || eventDetails.type=="click"){
    console.log("Entered");
  }
  this.auth.login(this.myForm.value).subscribe(
    (c) => {
      console.log(c);
      this.bg.token = c.token;
      const current = this.auth.getUserDetails();
      // console.log(current);
      // console.log(current['role']);
      console.log(current['role']);
      if(!current['role']){
        window.location.href=environment.adminUrl+"/auth/login";
        console.log("hello");
        return;
      }  else {
          this.bg.role = current['role'].split('-')
          window.location.href=environment.adminUrl+"/?token="+this.bg.token;

      }
      
        // return;
  //  }
   this.error="";

    },
    (error)=>
    {console.log(error)
      error="check once again"; 
    }
  )
  console.log(this.myForm.value);

}
}
