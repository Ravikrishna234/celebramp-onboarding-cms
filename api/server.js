const express = require('express');
const cors = require('cors');
const app = express();
const port = 5070;
const multer = require('multer');
const ejs = require('ejs');
var path = require('path'); 
var fileRoutes = require('./routes/file');
var fs = require('fs')
var fileonuploadname = ""
var bodyparser = require('body-parser');
const jsonParser = bodyparser.json({ limit: '5gb' });
var array = [];

const storage = multer.diskStorage({
    destination: async function(req,file,cb){
        console.log(file);
        console.log(file.originalname);
        var sp = file.originalname.split('.')
        file.originalname = fileonuploadname+"."+sp[1];
        console.log(file.originalname);
        var new_directory = `${file.originalname.split('.')[0]}`
        var dir = `public/uploads/`+new_directory
        // await fs.exists(dir,exist=>{
        //     console.log(exist)
        //     console.log("hello");
        //     if(!exist){
        //           fs.mkdirSync(dir)
        //          console.log(exist+"j");
                 
        //            cb(null,`./public/uploads/`+new_directory)
                 
                


        //     }else{
        //         cb(null,`./public/uploads/`+new_directory)
        //     }
        // })
        cb(null,`./public/uploads/`+new_directory)

       
    },
    filename:function(req,file, cb) {
        cb(null,Date.now()+ '.' + file.originalname)
    }
})
// const upload = multer({storage: storage});
app.use('/file',fileRoutes);

app.use(cors())

app.set('view engine','ejs')

app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
    res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PATCH, DELETE, OPTIONS');
    next();
  });
app.use(jsonParser);
app.use(express.json({ extended: true }));
app.use(express.urlencoded({ extended: true }));
  
app.use(express.static('./public'))

app.use(bodyparser.urlencoded({ extended: false }));

// function setname(req,res) {
// // var name = req.body;
// console.log(req);
// }
const upload = multer({storage: storage}).any('file');

app.post("/setname/:name", jsonParser,(req,res,next)=>{
    var dir = `public/uploads/`+req.params.name;
    fileonuploadname = req.params.name;
       fs.exists(dir,exist=>{
            console.log(exist)
            console.log("hello");
            if(!exist){
                  fs.mkdirSync(dir)
                 console.log(exist+"j");
                 res.status(200).json({'message':'created a directory'})
            }
        })
    console.log(req.params.name);

});

app.get('/',(req,res)=>res.render('index'))
// app.get('/',(req,res)=>{
//     console.log("hello");
// })
app.post('/upload',jsonParser,(req,res,next)=>{
    upload(req,res,function(err) {
        console.log(req.files);
        console.log("Hello");
        if(err) {
            return res.status(501).json({error:err});
            
        }
        array.push({PanCard:req.files[0].filename,
            AadharCard:req.files[1].filename,
            BussinessRegistrationDocument:req.files[2].filename,
            GstLicense:req.files[3].filename,

        }
            );
        console.log(array);
        var temp = [];
        temp = array.map((x) => x);

        // console.log(temp);
        array = [];
        // return res.json({originalName:req.file.originalname,uploadName:req.file.filename});
        return res.status(200).json({"message":temp})
        

        })
    })
app.post('/download',jsonParser,(req,res,next)=>{
    console.log("hello");
    console.log(req.body.filename);

    console.log(req.body.id);
    // res.send(true);

     filepath = path.join(__dirname,'../api/public/uploads/'+req.body.id) + '/' + req.body.filename;
     res.sendFile(filepath);

})

// app.post('/upload',upload.single('file'),(req,res)=>{
//     console.log(req.file);  
//     console.log("hai")
//     return res.json({originalName:req.file.originalname,uploadName:req.file.filename});

// })
app.listen(port,()=>console.log('listening on port '+port))

module.export = app;